var gulp = require('gulp'),
    notify = require('gulp-notify'), // �������� ��� .css � ���� ����
    // styles
    stylus = require('gulp-stylus'),
    concatCss = require('gulp-concat-css'), // �������� ��� .css � ���� ����
    minifyCss = require('gulp-minify-css'),
    // scripts
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),

    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect');

var path = {
    tracker: 'tracker',
    public: 'public'
};
path.assets = path.tracker + '/assets';
path.scripts = path.assets + '/scripts';
path.styles = path.assets + '/styles';
path.templates = path.tracker + '/templates';
path.images = path.assets + '/images';


// ��������� ������� �� ������� ������
gulp.task("lint", function() {
    return gulp.src(path.assets + "/scripts/**/*.js")
        .pipe(jshint())
        .pipe(jshint.reporter("default"));
});

// ������������ javascript ��� � ���� main.min.js
gulp.task('scripts', function () {
    gulp.src(path.scripts + '/**/*.js')
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(path.public + '/scripts'))
        .pipe(connect.reload())
});
//
//
//gulp.task('css', function () {
//    return gulp.src(path.styles + '/*.css')
//        .pipe(concatCss('styles/bundle.css'))
//        .pipe(minifyCss())
//        .pipe(rename('styles/bundle.min .css'))
//        .pipe(gulp.dest(path.public))
//        .pipe(notify('css: done!'));
//});
//
gulp.task('styles', function () {
    return gulp.src(path.styles + '/main.styl') // � main �� ���������� ������� �����������
        .pipe(stylus({compress: true}))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(path.public + '/styles'))
        .pipe(connect.reload())
});


gulp.task('index', function () {
    gulp.src(path.templates + '/index.html')
        .pipe(gulp.dest(path.public))
        .pipe(connect.reload())
});

gulp.task('watch', function () {
    gulp.watch(path.scripts + '/**/*.js', ['lint', 'scripts']);
    gulp.watch(path.styles + '/**/*.styl', ['styles']);
    gulp.watch(path.templates + '/**/*.html', ['index']);
});

gulp.task('connect', function() {

    connect.server({
        root: path.public,
        port: 8888,
        livereload: true
    });
});


gulp.task('default', ['lint', 'scripts', 'styles', 'index', 'watch', 'connect']);