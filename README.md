How to start
============

1. Install NodeJS `https://nodejs.org/`
The node installer also included `npm`
>
$ node -v
v0.12.6
$ npm -v
2.11.2


2. Clone project
>
git clone https://borls@bitbucket.org/borls/gulp-boiler.git

Go to the project `cd gulp-boiler`



History
=======
First commit
------------
1. `npm init` создаем package.json
2. `bower init` создаем bower.json

git init
git remote add origin https://borls@bitbucket.org/borls/gulp-boiler.git
git add -A .
git commit -m 'initial commit'
git push -u origin master


3. create `gulpfile.js`
```
npm i --save-dev 
     gulp 
     gulp-concat 
     gulp-concat-css 
     gulp-connect 
     gulp-jshint 
     gulp-livereload 
     gulp-minify-css 
     gulp-notify 
     gulp-rename 
     gulp-stylus
```

4. Styles Watcher
```File Watchers -> Stylus -> Program: C:\Users\yuzhakov\AppData\Roaming\npm\stylus.cmd```
